package com.stripe.interview

import akka.actor.ActorSystem
import com.typesafe.scalalogging.LazyLogging
import spray.client.pipelining.{Get, Post, addHeader, logRequest, logResponse}
import spray.http.HttpResponse
import spray.json._
import spray.json.DefaultJsonProtocol
import spray.client.pipelining._
import spray.http.HttpResponse
import scala.concurrent.duration._

import scala.concurrent.{Await, Future, Promise}
import scala.io.Source
import scala.util.{Failure, Success, Try}

object MyLog extends DefaultJsonProtocol{
  case class Request(val url:String, val headers: Map[String,String], body:String, method:String)
  case class Response(val body:String, val headers: Map[String,String], code:Int)
  case class ResponseRecvd(status:String)

  case class Pair(request:Request,response:Response)
  //only needed for the second question
  case class ExtractableResponse(`object`:String,id:String)

  implicit val responseFormat = jsonFormat3(Response.apply)
  implicit val requestFormat = jsonFormat4(Request.apply)
  implicit val pairFormat = jsonFormat2(Pair.apply)
  implicit val extractableResponse = jsonFormat2(ExtractableResponse.apply)
  implicit val responseRecvdFmt = jsonFormat1(ResponseRecvd)
}

import MyLog._
// import scalaj.http._

class HttpReq(val in:Pair,as:ActorSystem) extends LazyLogging{
  implicit val systeam = as
  import as.dispatcher
  type Entity = (Pair,HttpResponse)
  val supported=Seq("GET","POST")
  def fetch():Future[Entity] = {
    val p =Promise[Entity]
    var pipeline =
     logRequest(p=> logger.debug(p.toString)) ~>
      sendReceive~>
       logResponse(p=>logger.debug(p.toString))

    in.request.headers.foreach { p =>
      pipeline = addHeader(p._1, p._2) ~> pipeline
    }

    val url = "https://api.stripe.com"+ in.request.url
    Future{
      pipeline{
        if(in.request.method == "GET") Get(url)
        else Post(url+"?"+in.request.body)
      }.onComplete{
        case Success(s) =>
          p.success((in,s))
        case Failure(f) =>
          p.failure(f)
      }
    }
    p.future
  }
}


object Main extends LazyLogging{

  def main(args: Array[String]): Unit = {
    import scala.concurrent.ExecutionContext.Implicits.global
    val lines = Source.fromFile("urls.json").getLines.mkString
    val ll = lines.parseJson.convertTo[List[Pair]]

    val as = ActorSystem("Test")
    val reqs = ll.map(new HttpReq(_, as).fetch())
    Try{Await.result(Future.sequence(reqs),10.seconds)} match{
      case Success(s)=>
        for (r<-s){
          val received = r._2
          val expected = r._1.response
          if(expected.code != received.status.intValue) logger.error(s"status code doesn't match: expected: ${expected.code} ${received.status}")
          else logger.info(s"Status code matches for request: ${r._1.request.url}")
        }
      case Failure(f)=>
        println(f)
    }

    sys.exit(0)
  }
}
