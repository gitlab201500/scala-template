package problems

sealed trait List[+T]
case object Nil extends List[Nothing]
case class Ctor[T](head:T,tail:List[T]) extends List[T]

object List{
  def apply[A](as: A*) :List[A] = {
    if (as.isEmpty) Nil
    else Ctor(as.head, apply(as.tail: _*))
  }
}

object Main{
  def main(argv:Array[String]): Unit ={
    println(List(1,2,3))
  }
}