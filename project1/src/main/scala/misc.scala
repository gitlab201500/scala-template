package misc

import scala.annotation.tailrec

object Problems{


  def find(s:Seq[String],k:String) ={
    @tailrec
    def loop(s1:Seq[String], n:Int):Option[Int] = s1 match {
        case Seq() => None
        case h :: t => if (h == k) Some(n) else loop(t, n + 1)
      }
    loop(s,0)

  }

  def isSorted(s:Seq[String]):Boolean = {
    @tailrec
    def loop(s1:Seq[String], prev:String):Boolean = s1 match{
      case Seq() => true
      case h :: t => if (h<prev) false else loop(t,h)
    }
    s match {
      case Seq()=>true
      case h :: Seq() => true
      case h :: t => loop(t,h)
    }
  }


  def isSorted1(s:Seq[String]):Boolean = s.tail.foldLeft(if (s.headOption.isDefined) (true,s.head) else (true,"")){(p,c)=> p match {
    case (false,_)=>p
    case (true,pp) => if (pp>c) (false,pp) else (true,c)
  }}._1

  def curry[A,B,C](f:(A,B)=>C) : A=>(B=>C) = {
      def g(a: A):B=>C = {
        def g1(b:B):C = {
          f(a,b)
        }
        g1
      }
    g
  }

  def uncurry[A,B,C](f:A=>B=>C):(A,B)=>C ={
    (a,b)=>f(a)(b)
  }

  def main(argv:Array[String]) ={
    {
      println("findFirst")
      val a = Seq("a","b","c")
      println(find(a,"b"))
      println(find(a,"d"))
    }
    {
      val a = Seq("a","b","c")
      println("isSorted")
      println(isSorted(a))
      println(isSorted(Seq()))
      println(isSorted(Seq("a","c","b")))
    }
    {
      val a = Seq("a","b","c")
      println("isSorted1")
      println(isSorted(a))
      println(isSorted(Seq()))
      println(isSorted(Seq("a","c","b")))
    }

    {
      println("curry")
      def a (a:Int,b:Int) = a+b
      val sum: (Int, Int) => Int = _ + _
      val b = (curry(a))(1)
      def b2 = (curry(sum))(1)
      println(b(2))
      println(b2(2))
      val sumCurried = (sum.curried)(1)
      println(sumCurried(2))
      println(uncurry(sum.curried)(1,2))
    }
  }
}