package problems

import com.typesafe.scalalogging.LazyLogging


 case class Test(var v:String){
   v +="bbb"
   def t() = print(s"$v")
 }

object Main1 extends LazyLogging {

  def main(args: Array[String]): Unit = {
    Test("aa").t()
    sys.exit(0)
  }
}